package com.ezpay.subscription.intergrationTests;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest()
@AutoConfigureMockMvc
public class SubscriptionControllerTests {

    private final String BASE_URL = "/subscription";
    private final String BILL_CYCLE_URL = "/billCycle";

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testBillCycle_validWeeklySubscription_successWithExpectedData() throws Exception {
        // prepare
        String requestJson = "{ \"amount\": 10.0,\"subscriptionType\":2, \"dayOfWeek\" : \"FRIDAY\", \"startDate\":\"06/02/2018\",\"endDate\":\"27/02/2018\"}";
        var testResultJson = "{\"amount\":10.0,\"subscriptionType\":2,\"invoiceDate\":[\"06/02/2018\",\"13/02/2018\",\"20/02/2018\",\"27/02/2018\"]}";

        //perform
        var result = this.mockMvc
                .perform(post(BASE_URL+BILL_CYCLE_URL)
                            .content(requestJson)
                            .contentType(MediaType.APPLICATION_JSON));
        //Assert
        result.andExpect(status().isOk());
        result.andExpect(content().string(testResultJson));
    }

    @Test
    public void testBillCycle_validDailySubscription_successWithExpectedData() throws Exception {
        // prepare
        String requestJson = "{ \"amount\": 10.0,\"subscriptionType\":1 ,\"startDate\":\"06/02/2018\",\"endDate\":\"08/02/2018\"}";
        var testResultJson = "{\"amount\":10.0,\"subscriptionType\":1,\"invoiceDate\":[\"06/02/2018\",\"07/02/2018\",\"08/02/2018\"]}";

        //perform
        var result = this.mockMvc
                .perform(post(BASE_URL+BILL_CYCLE_URL)
                        .content(requestJson)
                        .contentType(MediaType.APPLICATION_JSON));
        //Assert
        result.andExpect(status().isOk());
        result.andExpect(content().string(testResultJson));
    }

    @Test
    public void testBillCycle_validMonthlySubscription_successWithExpectedData() throws Exception {
        // prepare
        String requestJson = "{ \"amount\": 10.0,\"subscriptionType\":3, \"dayOfMonth\":12 ,\"startDate\":\"06/02/2018\",\"endDate\":\"06/04/2018\"}";
        var testResultJson = "{\"amount\":10.0,\"subscriptionType\":3,\"invoiceDate\":[\"06/02/2018\",\"06/03/2018\",\"06/04/2018\"]}";

        //perform
        var result = this.mockMvc
                .perform(post(BASE_URL+BILL_CYCLE_URL)
                        .content(requestJson)
                        .contentType(MediaType.APPLICATION_JSON));
        //Assert
        result.andExpect(status().isOk());
        result.andExpect(content().string(testResultJson));
    }

    @Test
    public void testBillCycle_negativeAmount_returnBadRequest() throws Exception {
        // prepare
        String requestJson = "{ \"amount\": -0.01,\"subscriptionType\":1,\"startDate\":\"06/02/2018\",\"endDate\":\"06/04/2018\"}";

        //perform
        var result = this.mockMvc
                .perform(post(BASE_URL+BILL_CYCLE_URL)
                        .content(requestJson)
                        .contentType(MediaType.APPLICATION_JSON));
        //Assert
        result.andExpect(status().isBadRequest());
    }

    @Test
    public void testBillCycle_zeroAmount_returnOkSameResult() throws Exception {
        // prepare
        String requestJson = "{ \"amount\": 0,\"subscriptionType\":1,\"startDate\":\"06/02/2018\",\"endDate\":\"09/02/2018\"}";
        String exceptedResultJson = "{\"amount\":0,\"subscriptionType\":1,\"invoiceDate\":[\"06/02/2018\",\"07/02/2018\",\"08/02/2018\",\"09/02/2018\"]}";

        //perform
        var result = this.mockMvc
                .perform(post(BASE_URL+BILL_CYCLE_URL)
                        .content(requestJson)
                        .contentType(MediaType.APPLICATION_JSON));
        //Assert
        result.andExpect(status().isOk());
        result.andExpect(content().string(exceptedResultJson));
    }

    @Test
    public void testBillCycle_moreThan3Decimal_returnBadRequest() throws Exception {
        // prepare
        String requestJson = "{ \"amount\": 12.01646476,\"subscriptionType\":1 ,  \"startDate\":\"06/02/2018\",\"endDate\":\"09/02/2018\"}";

        //perform
        var result = this.mockMvc
                .perform(post(BASE_URL+BILL_CYCLE_URL)
                        .content(requestJson)
                        .contentType(MediaType.APPLICATION_JSON));
        //Assert
        result.andExpect(status().isBadRequest());
    }

    @Test
    public void testBillCycle_moreThan16Digit_returnBadRequest() throws Exception {
        // prepare
        String requestJson = "{ \"amount\": 5446464646464646464669886646464979797977979797.01,\"subscriptionType\":1,\"startDate\":\"06/02/2018\",\"endDate\":\"09/02/2018\"}";

        //perform
        var result = this.mockMvc
                .perform(post(BASE_URL+BILL_CYCLE_URL)
                        .content(requestJson)
                        .contentType(MediaType.APPLICATION_JSON));
        //Assert
        result.andExpect(status().isBadRequest());
    }

    @Test
    public void testBillCycle_nullDayOfMonthMonthlySubscription_returnBadRequest() throws Exception {
        // prepare
        String requestJson = "{ \"amount\": 10.0,\"subscriptionType\":3, \"dayOfMonth\":null ,\"startDate\":\"06/02/2018\",\"endDate\":\"06/04/2018\"}";

        //perform
        var result = this.mockMvc
                .perform(post(BASE_URL+BILL_CYCLE_URL)
                        .content(requestJson)
                        .contentType(MediaType.APPLICATION_JSON));
        //Assert
        result.andExpect(status().isBadRequest());
    }

    @Test
    public void testBillCycle_invalidDayOfMonthMonthlySubscription_returnBadRequest() throws Exception {
        // prepare
        String requestJson = "{ \"amount\": 12.01,\"subscriptionType\":3, \"dayOfMonth\":50, \"startDate\":\"06/02/2018\",\"endDate\":\"09/02/2018\"}";

        //perform
        var result = this.mockMvc
                .perform(post(BASE_URL+BILL_CYCLE_URL)
                        .content(requestJson)
                        .contentType(MediaType.APPLICATION_JSON));
        //Assert
        result.andExpect(status().isBadRequest());
    }

    @Test
    public void testBillCycle_nullDayOfWeekWeeklySubscription_returnBadRequest() throws Exception {
        // prepare
        String requestJson = "{ \"amount\": 10.0,\"subscriptionType\":2,\"startDate\":\"06/02/2018\",\"endDate\":\"06/04/2018\"}";

        //perform
        var result = this.mockMvc
                .perform(post(BASE_URL+BILL_CYCLE_URL)
                        .content(requestJson)
                        .contentType(MediaType.APPLICATION_JSON));
        //Assert
        result.andExpect(status().isBadRequest());
    }

    @Test
    public void testBillCycle_invalidDayOfWeekWeeklySubscription_returnBadRequest() throws Exception {
        // prepare
        String requestJson = "{ \"amount\": 12.01,\"subscriptionType\":3, \"dayOfWeek\":50, \"startDate\":\"06/02/2018\",\"endDate\":\"09/02/2018\"}";

        //perform
        var result = this.mockMvc
                .perform(post(BASE_URL+BILL_CYCLE_URL)
                        .content(requestJson)
                        .contentType(MediaType.APPLICATION_JSON));
        //Assert
        result.andExpect(status().isBadRequest());
    }

    @Test
    public void testBillCycle_invalidSubscriptionType_returnBadRequest() throws Exception {
        // prepare
        String requestJson = "{ \"amount\": 12.01,\"subscriptionType\":99 ,  \"startDate\":\"06/02/2018\",\"endDate\":\"09/02/2018\"}";

        //perform
        var result = this.mockMvc
                .perform(post(BASE_URL+BILL_CYCLE_URL)
                        .content(requestJson)
                        .contentType(MediaType.APPLICATION_JSON));
        //Assert
        result.andExpect(status().isBadRequest());
    }

    @Test
    public void testBillCycle_durationMoreThanMax_returnBadRequest() throws Exception {
        // prepare
        String requestJson = "{ \"amount\": 12.01,\"subscriptionType\":1 ,  \"startDate\":\"06/02/2018\",\"endDate\":\"09/12/2018\"}";

        //perform
        var result = this.mockMvc
                .perform(post(BASE_URL+BILL_CYCLE_URL)
                        .content(requestJson)
                        .contentType(MediaType.APPLICATION_JSON));
        //Assert
        result.andExpect(status().isBadRequest());
    }

    @Test
    public void testBillCycle_startBeforeEndDate_returnBadRequest() throws Exception {
        // prepare
        String requestJson = "{ \"amount\": 12.01,\"subscriptionType\":1 ,  \"startDate\":\"06/02/2019\",\"endDate\":\"09/12/2018\"}";

        //perform
        var result = this.mockMvc
                .perform(post(BASE_URL+BILL_CYCLE_URL)
                        .content(requestJson)
                        .contentType(MediaType.APPLICATION_JSON));
        //Assert
        result.andExpect(status().isBadRequest());
    }

    @Test
    public void testBillCycle_invalidDateTimeFormat_returnBadRequest() throws Exception {
        // prepare
        String requestJson = "{ \"amount\": 12.01,\"subscriptionType\":1 ,  \"startDate\":\"06-02-2019\",\"endDate\":\"09-12-2018\"}";

        //perform
        var result = this.mockMvc
                .perform(post(BASE_URL+BILL_CYCLE_URL)
                        .content(requestJson)
                        .contentType(MediaType.APPLICATION_JSON));
        //Assert
        result.andExpect(status().isBadRequest());
    }

    @Test
    public void testBillCycle_sameStartEndDate_returnOKValidResult() throws Exception {
        // prepare
        String requestJson = "{ \"amount\": 12.01,\"subscriptionType\":1 ,  \"startDate\":\"06/02/2018\",\"endDate\":\"06/02/2018\"}";
        String exceptedResult = "{\"amount\":12.01,\"subscriptionType\":1,\"invoiceDate\":[\"06/02/2018\"]}";

        //perform
        var result = this.mockMvc
                .perform(post(BASE_URL+BILL_CYCLE_URL)
                        .content(requestJson)
                        .contentType(MediaType.APPLICATION_JSON));
        //Assert
        result.andExpect(status().isOk());
        result.andExpect(content().string(exceptedResult));
    }


}
