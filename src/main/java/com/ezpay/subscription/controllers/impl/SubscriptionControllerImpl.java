package com.ezpay.subscription.controllers.impl;

import com.ezpay.subscription.controllers.SubscriptionController;
import com.ezpay.subscription.dto.input.SubscriptionDetail;
import com.ezpay.subscription.dto.output.BillCycles;
import com.ezpay.subscription.services.SubscriptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.Valid;

@Component
public class SubscriptionControllerImpl implements SubscriptionController {

    private final SubscriptionService subscriptionService;

    @Autowired
    public SubscriptionControllerImpl(SubscriptionService subscriptionService) {
        this.subscriptionService = subscriptionService;
    }

    @Override
    public BillCycles getSubscriptionBillCycle(@Valid SubscriptionDetail subscription) {

      return this.subscriptionService.getBillCycleDate(subscription);
    }
}
