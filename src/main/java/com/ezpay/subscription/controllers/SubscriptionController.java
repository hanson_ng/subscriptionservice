package com.ezpay.subscription.controllers;

import com.ezpay.subscription.dto.input.SubscriptionDetail;
import com.ezpay.subscription.dto.output.BillCycles;
import org.springframework.web.bind.annotation.*;

@RequestMapping(value = "/subscription")
@RestController
public interface SubscriptionController {

    @PostMapping("/billCycle")
    BillCycles getSubscriptionBillCycle(@RequestBody SubscriptionDetail subscription);
}
