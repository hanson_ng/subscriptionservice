package com.ezpay.subscription.services.impl;

import com.ezpay.subscription.domain.subscription.SubscriptionAbstractFactory;
import com.ezpay.subscription.dto.input.SubscriptionDetail;
import com.ezpay.subscription.dto.output.BillCycles;
import com.ezpay.subscription.services.SubscriptionService;
import org.springframework.stereotype.Component;

@Component
public class SubscriptionServiceImpl implements SubscriptionService {

    @Override
    public BillCycles getBillCycleDate(SubscriptionDetail subscriptionDetail) {
        var subscription = SubscriptionAbstractFactory.create(subscriptionDetail);
        return new BillCycles()
                .toBuilder()
                .amount(subscriptionDetail.getAmount())
                .subscriptionType(subscriptionDetail.getSubscriptionType())
                .invoiceDate(subscription.getBillCycleDates())
                .build();
    }
}
