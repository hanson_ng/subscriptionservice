package com.ezpay.subscription.services;

import com.ezpay.subscription.dto.input.SubscriptionDetail;
import com.ezpay.subscription.dto.output.BillCycles;
import org.springframework.stereotype.Service;

import java.security.InvalidParameterException;

@Service
public interface SubscriptionService {

    BillCycles getBillCycleDate(SubscriptionDetail subscriptionDetail) throws InvalidParameterException;

}
