package com.ezpay.subscription.validators.interfaces;

import com.ezpay.subscription.validators.subscriptiondetaildto.DayValidator;
import com.ezpay.subscription.validators.subscriptiondetaildto.DurationValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = DayValidator.class)
public @interface DayValidate {
    String message() default "DaysOfWeek/MonthOfWeek is empty" ;

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
