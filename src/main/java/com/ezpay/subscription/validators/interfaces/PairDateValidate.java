package com.ezpay.subscription.validators.interfaces;

import com.ezpay.subscription.validators.subscriptiondetaildto.PairDateValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = PairDateValidator.class)
public @interface PairDateValidate {

    String message() default "End date cannot before Start Date";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
