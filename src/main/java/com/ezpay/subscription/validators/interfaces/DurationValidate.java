package com.ezpay.subscription.validators.interfaces;


import com.ezpay.subscription.validators.subscriptiondetaildto.DurationValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.time.temporal.ChronoUnit;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = DurationValidator.class)
public @interface DurationValidate {

    String message() default "Duration cannot over max" ;

    ChronoUnit unit() default ChronoUnit.MONTHS;

    long amountToAdd() default  3;

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

