package com.ezpay.subscription.validators.subscriptiondetaildto;

import com.ezpay.subscription.dto.input.SubscriptionDetail;
import com.ezpay.subscription.validators.interfaces.PairDateValidate;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PairDateValidator implements ConstraintValidator<PairDateValidate, SubscriptionDetail> {
    @Override
    public boolean isValid(SubscriptionDetail subscriptionDetail, ConstraintValidatorContext constraintValidatorContext) {
            return subscriptionDetail.getEndDate().isAfter(subscriptionDetail.getStartDate())
                    || subscriptionDetail.getEndDate().isEqual(subscriptionDetail.getStartDate()) ;
    }
}
