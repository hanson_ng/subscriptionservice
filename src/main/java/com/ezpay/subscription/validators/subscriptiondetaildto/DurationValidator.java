package com.ezpay.subscription.validators.subscriptiondetaildto;

import com.ezpay.subscription.dto.input.SubscriptionDetail;
import com.ezpay.subscription.validators.interfaces.DurationValidate;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.temporal.ChronoUnit;

public class DurationValidator implements ConstraintValidator<DurationValidate, SubscriptionDetail> {

    private long amountToAdd;
    private ChronoUnit unit;

    @Override
    public void initialize(DurationValidate constraintAnnotation) {
        this.amountToAdd = constraintAnnotation.amountToAdd();
        this.unit = constraintAnnotation.unit();
    }

    @Override
    public boolean isValid(SubscriptionDetail subscriptionDetail, ConstraintValidatorContext constraintValidatorContext) {

        return subscriptionDetail.getEndDate().isBefore(subscriptionDetail.getStartDate().plus(amountToAdd, unit));
    }
}
