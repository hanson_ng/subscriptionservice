package com.ezpay.subscription.validators.subscriptiondetaildto;

import com.ezpay.subscription.dto.input.SubscriptionDetail;
import com.ezpay.subscription.validators.interfaces.DayValidate;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class DayValidator implements ConstraintValidator<DayValidate, SubscriptionDetail> {

    @Override
    public boolean isValid(SubscriptionDetail subscriptionDetail, ConstraintValidatorContext constraintValidatorContext) {
        var subscriptionType = subscriptionDetail.getSubscriptionType();

        if(subscriptionType == null) {
            return false;
        }

        switch (subscriptionType){
            case Weekly:
                var day = subscriptionDetail.getDayOfWeek();
                return day != null;
            case Monthly:
                var dayOfMonth = subscriptionDetail.getDayOfMonth();
                return dayOfMonth != null  && dayOfMonth > 0 && dayOfMonth < 31;
            default:
                return true;
        }
    }
}
