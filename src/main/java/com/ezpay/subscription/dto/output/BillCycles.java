package com.ezpay.subscription.dto.output;

import com.ezpay.subscription.common.Format;
import com.ezpay.subscription.common.enums.SubscriptionType;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.*;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Data
@ToString
@Builder(toBuilder = true)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonSerialize
@EqualsAndHashCode
public class BillCycles {

    private BigDecimal amount;

    private SubscriptionType subscriptionType;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Format.DATE_FORMAT)
    private List<LocalDate> invoiceDate;
}
