package com.ezpay.subscription.dto.input;

import com.ezpay.subscription.common.LocalDateDeserializer;
import com.ezpay.subscription.common.Format;
import com.ezpay.subscription.common.enums.SubscriptionType;
import com.ezpay.subscription.validators.interfaces.DayValidate;
import com.ezpay.subscription.validators.interfaces.PairDateValidate;
import com.ezpay.subscription.validators.interfaces.DurationValidate;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.*;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.DayOfWeek;
import java.time.LocalDate;


@Data
@ToString
@Getter
@Setter
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@DurationValidate
@PairDateValidate
@DayValidate
@EqualsAndHashCode
public class SubscriptionDetail {

    @NotNull
    @DecimalMin( value = "0.0")
    @Digits(integer = 16, fraction = 3)
    private BigDecimal amount;

    @NotNull()
    private SubscriptionType subscriptionType;

    private Integer dayOfMonth;

    private DayOfWeek dayOfWeek;

    @NotNull
    @JsonDeserialize(using = LocalDateDeserializer.class)
    private LocalDate startDate;

    @NotNull
    @JsonDeserialize(using = LocalDateDeserializer.class)
    private LocalDate endDate;
}
