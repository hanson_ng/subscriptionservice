package com.ezpay.subscription.domain.subscription;

import com.ezpay.subscription.domain.Subscription;
import com.ezpay.subscription.dto.input.SubscriptionDetail;

import java.time.LocalDate;
import java.time.temporal.TemporalUnit;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractSubscription implements Subscription {

    protected final SubscriptionDetail subscriptionDetail;

    protected AbstractSubscription(SubscriptionDetail subscriptionDetail){
        this.subscriptionDetail = subscriptionDetail;
    }

    @Override
    public List<LocalDate> getBillCycleDates() {
        var listOfInvoiceDate = new ArrayList<LocalDate>();
        var startDate = this.subscriptionDetail.getStartDate();
        var endDate = this.subscriptionDetail.getEndDate();
        var nextCycle = startDate;

        do {
            listOfInvoiceDate.add(nextCycle);
            nextCycle = nextCycle.plus(getAmountToAdd(), getTemporalUnit());
        } while (!nextCycle.isAfter(endDate));

        return listOfInvoiceDate;
    }

    protected abstract TemporalUnit getTemporalUnit();

    protected abstract long getAmountToAdd();
}
