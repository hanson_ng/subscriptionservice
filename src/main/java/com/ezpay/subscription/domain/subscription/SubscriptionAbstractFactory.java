package com.ezpay.subscription.domain.subscription;

import com.ezpay.subscription.domain.Subscription;
import com.ezpay.subscription.dto.input.SubscriptionDetail;

import java.security.InvalidParameterException;

public class SubscriptionAbstractFactory {

    private SubscriptionAbstractFactory(){}

    public static Subscription create(SubscriptionDetail subscriptionDetail) throws InvalidParameterException {
        switch (subscriptionDetail.getSubscriptionType()){
            case Daily:
                return new DailySubscription(subscriptionDetail);
            case Monthly:
                return new MonthlySubscription(subscriptionDetail);
            case Weekly:
                return new WeeklySubscription(subscriptionDetail);
        }
        throw new InvalidParameterException("Invalid Subscription Type");
    }
}
