package com.ezpay.subscription.domain.subscription;

import com.ezpay.subscription.dto.input.SubscriptionDetail;

import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;

public class DailySubscription extends AbstractSubscription {

    protected final static TemporalUnit TEMPORAL_UNIT = ChronoUnit.DAYS;
    protected final static long AMOUNT_TO_ADD = 1;

    public DailySubscription(SubscriptionDetail subscriptionDetail) {
        super(subscriptionDetail);
    }

    @Override
    protected TemporalUnit getTemporalUnit() {
        return TEMPORAL_UNIT;
    }

    @Override
    protected long getAmountToAdd() {
        return AMOUNT_TO_ADD;
    }
}
