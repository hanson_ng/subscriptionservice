package com.ezpay.subscription.domain;

import com.ezpay.subscription.common.enums.SubscriptionType;

import java.time.LocalDate;
import java.util.List;

public interface Subscription {
    List<LocalDate> getBillCycleDates();
}
