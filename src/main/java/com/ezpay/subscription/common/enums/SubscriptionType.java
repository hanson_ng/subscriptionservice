package com.ezpay.subscription.common.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.stream.Stream;

public enum SubscriptionType {
    Daily(1),
    Weekly(2),
    Monthly(3);

    private final int code;

    SubscriptionType(int code) {
        this.code = code;
    }

    @JsonCreator
    public static SubscriptionType decode(final int code) {
        return Stream.of(SubscriptionType.values())
                .filter(target -> target.code == code)
                .findFirst()
                .orElse(null);
    }

    @JsonValue
    public int getCode(){
        return this.code;
    }
}
